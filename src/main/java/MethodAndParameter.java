
public class MethodAndParameter {
    public static void main(String[] args) {
        String s;
        s = m("первый", 2);
        System.out.println(s);
    }

    static String m(String par1, int par2) {
        String result;
        result = "Параметр " + par1 + ", параметр " + par2 + "-ой";
        return result;
    }
}